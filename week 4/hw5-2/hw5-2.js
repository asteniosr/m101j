/* **
   * Please calculate the average population of cities in California (abbreviation CA) and New York (NY) (taken together) with populations over 25,000.
   * 
   * For this problem, assume that a city name that appears in more than one state represents two separate cities.
   * 
   * Please round the answer to a whole number.
   * Hint: The answer for CT and NJ is 49749. 
   * **/ 

db.zips.aggregate([
	{$match:{state:{$in:["CA", "NY"]}}}
	,{$group:{_id:{state:"$state", city:"$city"}, pop:{$sum:"$pop"}}}
	,{$match:{pop:{$gt:25000}}}
	,{$group:{_id:0,pop:{$avg:"$pop"}}}
])
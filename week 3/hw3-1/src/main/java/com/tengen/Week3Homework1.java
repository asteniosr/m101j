/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import java.net.UnknownHostException;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class Week3Homework1 {

	/*
	 * This dataset holds the same type of data as last week's grade collection, but it's modeled differently. You might want to start by inspecting it in the Mongo shell.
	 * 
	 * Write a program in Java that will remove the lowest homework score for each student. Since there is a single document for each student containing an array of scores, you
	 * will need to update the scores array and remove the homework.
	 * 
	 * Hint/spoiler: With the new schema, this problem is a lot harder and that is sort of the point. One way is to find the lowest homework in code and then update the scores
	 * array with the low homework pruned. If you are struggling with the Java side of this, look at the ArrayList.remove method, which can remove stuff from a Java ArrayList. Note
	 * also that when the value for a particular key is list within MongoDB, the Java driver returns a BasicDBList, which can you cast to an ArrayList.
	 */

	public static void main(String[] args) throws UnknownHostException {
		MongoClient client = new MongoClient();

		DB database = client.getDB("school");
		DBCollection collection = database.getCollection("students");

		DBCursor cursor = collection.find();

		while (cursor.hasNext()) {
			DBObject student = cursor.next();

			List<DBObject> scores = (List<DBObject>) student.get("scores");

			DBObject minimalHomeWorkScore = minimalHomeWork(scores);
			collection.update(new BasicDBObject("_id", student.get("_id")), 
							  new BasicDBObject("$pull", new BasicDBObject("scores", minimalHomeWorkScore)), 
							  true, false);

		}

	}

	private static DBObject minimalHomeWork(List<DBObject> scores) {
		DBObject minimalHomeWork = null;
		for (DBObject score : scores) {
			if (score.get("type").equals("homework")) {
				minimalHomeWork = minimalScore(minimalHomeWork, score);
			}
		}
		return minimalHomeWork;
	}

	private static DBObject minimalScore(DBObject oldScore, DBObject newScore) {

		if (oldScore == null)
			return newScore;

		if (newScore == null)
			return oldScore;

		Double oldscore = (Double) oldScore.get("score");
		Double newscore = (Double) newScore.get("score");

		if (Double.compare(oldscore, newscore) > 0)
			return newScore;
		return oldScore;
	}
}

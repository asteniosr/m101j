/* Write a program in the language of your choice that will remove the grade
/* of type "homework" with the lowest score for each student from the dataset
/* that you imported in HW 2.1. Since each document is one grade, it should
/* remove one document per student. */


/* Hint/spoiler: If you select homework grade-documents, sort by student and
/* then by score, you can iterate through and find the lowest score for each
/* student by noticing a change in student id. As you notice that change of
/* student_id, remove the document.*/

cur = db.grades.find({type:"homework"}).sort({"student_id":1, "score":1}); null;
id = -1;
while(cur.hasNext()){
	doc = cur.next(); null;
	if (id != doc.student_id) {
		id = doc.student_id;
		doc;
	}
}
/*
 * Copyright (c) 2008 - 2013 10gen, Inc. <http://10gen.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tengen;

import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class Week2Homework2 {

	/*
	 * Write a program in the language of your choice that will remove the grade of type "homework" with the lowest score for each student from the dataset that you imported in HW
	 * 2.1. Since each document is one grade, it should remove one document per student.
	 */

	/*
	 * Hint/spoiler: If you select homework grade-documents, sort by student and then by score, you can iterate through and find the lowest score for each student by noticing a
	 * change in student id. As you notice that change of student_id, remove the document.
	 */

	public static void main(String[] args) throws UnknownHostException {
		MongoClient client = new MongoClient();

		DB database = client.getDB("students");
		DBCollection collection = database.getCollection("grades");

		// Not necessary yet to understand this. It's just to prove that you
		// are able to run a command on a mongod server

		DBCursor cur = collection.find(new BasicDBObject("type", "homework")).sort(new BasicDBObject("student_id", 1).append("score", 1));
		Integer id = -1;
		while (cur.hasNext()) {
			DBObject doc = cur.next();
			Integer current_id = (Integer) doc.get("student_id");
			if (!id.equals(current_id)) {
				id = current_id;
				collection.remove(doc);
			}
		}

	}
}
